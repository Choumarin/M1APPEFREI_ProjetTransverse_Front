import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'

import Home from './Home';
import Blockument from './Blockument';
import Login from './Login';
import Create from './Create';
import NotFoundPage from './NotFoundPage';
import BlockumentDetails from './BlockumentDetails';
import { isMedecin } from './utils'


import logo from '../logo.png';


class App extends React.Component {
 constructor(props) {
    super(props);
    this.state = { token: null };
  }

  render() {
      return (
      <Router>
        <div className='App'>
          <header className="App-header">
            <div className='container' style={{marginTop: '-4%'}}>
              <a href="home"> 
                <img src={logo} className="blur_on" alt="logo" style={{width: '40%',
                                                                      minWidth: '20em'}}/>
              </a>
            </div>
          </header>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/home' component={Home} />
            <Route exact path='/blockumentdetails' render={() => (sessionStorage.getItem('token')===null ? (<Redirect to="/login"/>) : (<BlockumentDetails/>))} /> 
            <Route exact path='/blockument' render={() => (sessionStorage.getItem('token')===null ? (<Redirect to="/login"/>) : (<Blockument/>))}/>
            <Route path='/login' render={() => (sessionStorage.getItem('token') ===null ?   (<Login/>) : (<Redirect to="/blockument"/>))}/>
            <Route path='/create' render={() => (sessionStorage.getItem('token')===null ? (<Redirect to="/login"/>) : isMedecin() ? (<Create/>) : (<NotFoundPage/>))}/>
            <Route component={NotFoundPage}/>
          </Switch>
        </div>
      </Router>
    )
  }
}

export default App;