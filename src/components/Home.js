import login from '../images/login.png'
import blockument from '../images/blockument_list.png'
import choose from '../images/choose.png'
import create from '../images/create.png'

var React = require('react');

class Home extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
  	  role: '',
  	  name: '',
  	  lastname: '',
  	  error: null,
        isLoaded: false,
        items: [],
  	  isLoginTab: true,
  	  isValid: true,
  	  isCompleted: true,
	 };
  }

  render() {
    return (
      <div className='blockument'>
        <nav>
          <div className='nav-wrapper teal lighten-5 row'>
            <ul  className='left'>
               {sessionStorage.getItem('token')===null ? '' : <li><a href='blockument' className='teal-text'>Mes Blockuments</a></li>}
            </ul>
            <ul className='right'>
                {sessionStorage.getItem('token')===null ? <li><a href='login' className='teal-text'>Connexion</a></li>:<li><a onClick={this.logOut} className='teal-text'>Déconnexion</a></li>}
                {sessionStorage.getItem('token')===null ? <li><a href='login' className='teal-text'>Créer un compte</a></li>:null}
            </ul>
          </div>
        </nav>
        <div class='margin-top-6'>
          <h5 class='teal-text'>Bienvenue sur notre visite guidée</h5>
          <div class='container margin-top-6'>
            <div class='left-align'>
              <p>Grâce à Blockument, vous pourrez dès aujourd'hui vous assurer de la validité et du caractère privé de vos données personnelles.</p>
              <p>
                Le Blockument est un document certifié et suivi. 
                Grâce à notre application vous pourrez désormais savoir avec certitude qui a lu ou modifié votre Blockument. 
                Aussi, vous pourrez partager vos Blockument aux personnes de notre communauté en toute simplicité.
              </p>
            </div>
            <div class='margin-top-4 row left-align'>
              <div class='col m7 s7'>
                <img src={login} className='blur_on prefix z-depth-2' alt='login-img' style={{width: '100%'}}/>
              </div>
              <div class='col m5 s5'>
                <p>
                  Commencez par vous créer un compte. 
                  Il est nécessaire que vous indiquiez votre statut.
                </p>
                <p> 
                  Si vous êtes un professionnel de la santé, votre identité devra être vérifiée avant la validation de votre compte.
                </p>
              </div>
            </div>
            <div class='margin-top-4 row right-align'>
              <div class='col m5 s5'>
                <p>
                  Vous aurez ainsi accès à votre liste de Blockument.
                </p>
                <p>
                  Vous pourrez ainsi voir le créateur et la date de dernière modification du Blockument.
                </p>
              </div>
              <div class='col m7 s7'>
                <img src={blockument} className='blur_on prefix z-depth-2' alt='login-img' style={{width: '100%'}}/>
              </div>
            </div>
            <div class='margin-top-4 row left-align'>
              <div class='col m7 s7'>
                <img src={choose} className='blur_on prefix z-depth-2' alt='login-img' style={{width: '100%'}}/>
              </div>
              <div class='col m5 s5'>
                <p>
                  Pour créer un Blockument, vous choisirez dans un premier temps le type du Blockument.
                </p>
                <p>
                  Si vous êtes un professionnel de la santé vous pourrez créer un compte-rendu, un certificat médical ou une ordonnance.
                </p>
              </div>
            </div>
            <div class='margin-top-4 row right-align'>
              <div class='col m5 s5'>
                <p>
                  Il ne vous reste plus qu'à remplir le formulaire permettant la création du Blockument. 
                  Une fois validé, vous pourrez y accéder en toute sécurité.
                </p>
              </div>
              <div class='col m7 s7'>
                <img src={create} className='blur_on prefix z-depth-2' alt='login-img' style={{width: '100%'}}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

    logOut() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('password');
    window.location.reload()
  }
}



export default Home;