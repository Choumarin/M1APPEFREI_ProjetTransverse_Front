import React from 'react';	

import {API_BLOCKUMENTS} from '../config';
import {CHECK_BLOCKCHAIN} from '../config';
import {NOTIFY_DOWNLOAD} from '../config';
import {SET_SHARED} from '../config';

import { convertTimestamp } from './utils';

class BlockumentDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: window.location.href.split("#")[1],
      items: null,
      items_blockchain:null,
    };
      this.getBlockumentDetails();
    }

    getBlockumentDetails() {
	    let data = {
	      'username':sessionStorage.getItem('username'),
	      'password': sessionStorage.getItem('password'),
	      'blockument_id': this.state.id
	    };

	    fetch(API_BLOCKUMENTS, {
	      method: 'POST',
	      headers: {
	        'Accept': 'application/json',
	        'Content-Type': 'application/json',
	      },                
	      body: JSON.stringify(data)
	    }).then(
	      res => res.json(),
	    ).then(
	      (result) => {
	      	//console.log(result);
	      	result.blockument.content = JSON.parse(result.blockument.content);
	        this.setState({block : result})        
	      },
	      (error) => {
	        this.setState({error: error.message})
	      }
	    );


	   fetch(CHECK_BLOCKCHAIN, {
	      method: 'POST',
	      headers: {
	        'Accept': 'application/json',
	        'Content-Type': 'application/json',
	      },                
	      body: JSON.stringify(data)
	    }).then(
	      res => res.json(),
	    ).then(
	      (result_blockchain) => {
	      	console.log(result_blockchain);
	        this.setState({block_history : result_blockchain})        
	      },
	      (error) => {
	        this.setState({error: error.message})
	      }
	    );
  }

    download() {
    	let data = {
	      'username':sessionStorage.getItem('username'),
	      'password': sessionStorage.getItem('password'),
	      'blockument_id': window.location.href.split("#")[1]
	    };

	    fetch(NOTIFY_DOWNLOAD, {
	      method: 'POST',
	      headers: {
	        'Accept': 'application/json',
	        'Content-Type': 'application/json',
	      },                
	      body: JSON.stringify(data)
	    });

  }

 	share() {
        var username_shared = prompt("Avec quel patient ?:", "")
    	let data = {
	      'username':sessionStorage.getItem('username'),
	      'password': sessionStorage.getItem('password'),
	      'blockument_id': window.location.href.split("#")[1],
	      'username_shared' : username_shared,
	    };

	    fetch(SET_SHARED, {
	      method: 'POST',
	      headers: {
	        'Accept': 'application/json',
	        'Content-Type': 'application/json',
	      },                
	      body: JSON.stringify(data)
	    });
  }



  render() {
    return ( 
    <div className='blockument'>
         <nav>
            <div className='nav-wrapper teal lighten-5'>
                <ul className='left'>
                    <li><a className='teal-text' href="blockument"><i className="material-icons left icon-teal">arrow_back</i></a></li>

            	</ul>
            	<ul id='nav-mobile' className='right'>
              		<li><a className='teal-text' onClick={this.logOut}>Déconnexion</a></li>
            	</ul>
          	</div>
        </nav>
  		<div className="row teal lighten-4">
  			<div className="col m8 s12">
  				<div className='z-depth-3' style={{height: '1170px', 
  							width: '827px',
  							backgroundColor: 'white',
  							margin : '1em'}}>
  					<div className='container padding-top-6'>
  						{this.state.block == null ? '' : this.getBlockument()}
  					</div>
  				</div>
  			</div>
  			<div className="menu-detail col m4 s12">
  				<div className="row">
  					<div className="col s6">
  						<span className="context-menu col s12 teal-text margin-bottom-1" >
  							<div className='valign-wrapper'>
  								<i className="medium material-icons left icon-teal">person</i>
  								<div className='left-align' style={{fontSize: '10px',
    															lineHeight: '17px'}}>
	  								Créateur<br/>
	  								<b style={{fontSize: '20px'}}>{this.state.block == null ? '' : "Dr. " + this.state.block.blockument.author.lastname}</b><br/>
	  								{this.state.block == null ? '' : this.state.block.blockument.author.role}
								</div>
							</div>
  						</span>
  						<span className="context-menu col s12 teal-text margin-bottom-1">
  							<div className='valign-wrapper'>
  								<i className="medium material-icons left icon-teal">access_time</i>
  								<div className='left-align' style={{fontSize: '10px',
    															lineHeight: '17px'}}>
	  								Date de création<br/>
	  								<b style={{fontSize: '20px'}}>{this.state.block==null? '' : convertTimestamp(this.state.block.blockument.creation_date)}</b><br/>
								</div>
							</div>
  						</span>
  						<span className="context-menu col s12 teal-text margin-bottom-1">
  							<div className='valign-wrapper'>
  								<i className="medium material-icons left icon-teal">refresh</i>
  								<div className='left-align' style={{fontSize: '10px',
    															lineHeight: '17px'}}>
	  								Dernière modification<br/>
	  								<b style={{fontSize: '20px'}}>{this.state.block==null? '' : convertTimestamp(this.state.block.blockument.last_update_date)}</b><br/>
	  								{this.state.block==null? '' : this.state.block.blockument.last_update_author.firstname + " " + this.state.block.blockument.last_update_author.lastname}
								</div>
							</div>
  						</span>
  						<span className="context-menu col s12 teal-text margin-bottom-1">
  							<div className=''>
  								<i className="medium material-icons left icon-teal">remove_red_eye</i>
  								<div className='left-align' style={{fontSize: '10px',
    															lineHeight: '17px'}}>
	  								Nombre de vues<br/>
	  								<b style={{fontSize: '20px'}}>{this.state.block_history==null? '' : this.state.block_history.history.nb_reads}</b><br/><br/>
	  								Utilisateurs : <br/>

	  								<div className = "scroll">
	  									{this.state.block_history==null? '' : this.state.block_history.history.reads_list.map(function(reader, i) {
						  					return (
						    					<span  key={i}>- {reader.author} {convertTimestamp(reader.date)}<br/></span>
						  					)
										})}
									</div>
								</div>
							</div>
  						</span>			
  					</div>
  					<div className="col s6">
  						<a className='pointer-mouse valign-wrapper margin-top-1 margin-bottom-2' onClick={this.download}>
							<i className="medium material-icons left icon-teal">cloud_download</i>
							<b className='teal-text' style={{fontSize: '20px'}}>Télécharger</b>
						</a>
						<a className='pointer-mouse valign-wrapper margin-top-3 margin-bottom-2' onClick={this.share}>
							<i className="medium material-icons left icon-teal">share</i>
							<b className='teal-text' style={{fontSize: '20px'}}>Partager</b>
						</a>
						{/* <a className='valign-wrapper margin-top-3 margin-bottom-2' onClick={''}>
							<i class="medium material-icons left icon-teal">edit</i>
							<b className='teal-text' style={{fontSize: '20px'}}>Modifier</b>
						</a> */}
  					</div>
  				</div>
  			</div>      
  		</div>
  	</div>
    );
  }

  getBlockument() {
  	console.log(this.state.block);
  	return (
  		<div>
	  		<div className='row'>
	  			<div className='col m4 s4 left-align'>
					{this.getDoctorDetails()}
				</div>
				<div className='col offset-m4 m4 offset-s4 s4 left-align'>
					{this.getContactDetails()}
				</div>
			</div>
			<div className='margin-top-5 right-align' style={{fontSize: '10px',
														color: '#109C8F'}}>
				Paris, le {convertTimestamp(this.state.block.blockument.last_update_date)}
			</div>
			<div className='margin-top-2 left-align' style={{fontSize: '10px'}}>
				<p>
					Monsieur ou Madame {(this.state.block.blockument.content.lastname).toUpperCase() + ' ' + this.state.block.blockument.content.firstname + ','}
					<br/>
					{this.state.block.blockument.content.age === '' ? '' : this.state.block.blockument.content.age + ' ans '}
				</p>
				<p className='margin-top-5'>
					{this.state.block.blockument.content.order.prescription.map( (prescription) => {
						if(prescription.drug !== '') {
							let moment = '';
							if(prescription.moment.morning) {
								moment = moment + ' matin';
							}
							if(prescription.moment.midday) {
								moment = moment + ' midi';
							}
							if(prescription.moment.evening) {
								moment = moment + ' soir';
							}

							let period ='';
							if(prescription.period === 'day') {
								period = ' jour(s)';
							}
							if(prescription.period === 'week') {
								period = ' semaine(s)';
							}
							if(prescription.period === 'month') {
								period = ' mois';
							}

							return (
								<p>{(prescription.drug).toUpperCase() + ' x' + prescription.quantity + moment + ' pendant ' + prescription.duration + period}</p>
							)
						}}
					)}
					<br/><br/>
					{this.state.block.blockument.content.report === '' ? '' : this.state.block.blockument.content.report.report}
					{this.state.block.blockument.content.order.advice === '' ? '' : this.state.block.blockument.content.order.advice}
				</p>
			</div>
		</div>
  	)
  }

  getContactDetails() {
  	return (
  		<div style={{fontSize: '10px',
  					color: '#109C8F'}}>
	  		<b><p>1 rue du la Paix,<br/>75016 Paris</p></b>
	  		<p>Tél. cabinet : 01 23 45 67 89<br/>Tél. urgence : 06 98 76 54 43</p>
  		</div>
  	);
  }

  getDoctorDetails() {
  	return (
  		<div style={{fontSize: '10px',
  					color: '#109C8F'}}>
	  		<b><p>Docteur {this.state.block.blockument.author.lastname}<br/>Médecin généraliste</p></b>
	  		<p>Diplômé à Paris</p>
  		</div>
  	);
  }



  logOut() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('password');
    window.location.reload()
  }
}

export default BlockumentDetails;