var React = require('react');
var Link = require('react-router-dom').Link;

class NotFoundPage  extends React.Component {
    render(){
        return (
			<div>
				<h1>Page Not Found</h1>
				<p>Désolé, la page que vous voulez consulter n'existe pas.</p>
				<p><Link to="/home">Retour Home</Link></p>
			</div>
		);
    }
}

export default NotFoundPage;