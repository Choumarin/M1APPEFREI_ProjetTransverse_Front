var jwtDecode = require('jwt-decode');


export function convertTimestamp(timestamp) {
        var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
            dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
            h = d.getHours(),
            min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
            time;

        // ie: 2013-02-18, 8:35	
        time = dd + '-' + mm + '-' + yyyy + ' ' + h + ':' + min;

        return time;
        }

export function isMedecin(){
        var decode = (jwtDecode(sessionStorage.getItem('token')));
        let user = JSON.parse(decode.user)
        return user.role === "medecin" ? true : false;
  } 

  export function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}