import React from 'react';
import  { Route, Redirect} from 'react-router-dom'
import {API_LOGIN} from '../config';
import {API_SIGNUP} from '../config';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import { capitalizeFirstLetter } from './utils';


var jwtDecode = require('jwt-decode');
window.M.AutoInit();


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            confirmed_password: '',
            name: '',
            lastname: '',
            email: '',
            role: '',
            error: null,
            isLoaded: false,
            items: null,
            isLoginTab: true,
            isValid: true,
            isCompleted: true,
            loggedIn: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        console.log("handlechange");
        switch (event.target.id) {
            case 'username':
                this.setState({username: event.target.value});
                break;
            case 'password':
                this.setState({password: event.target.value});
                break;
            case 'confirmed_password':
                this.setState({confirmed_password: event.target.value});
                break;
            case 'name':
                this.setState({name: event.target.value});
                break;
            case 'lastname':
                this.setState({lastname: event.target.value});
                break;
            case 'email':
                this.setState({email: event.target.value});
                break;
            case 'role':
                this.setState({role: event.target.value});
                console.log("role"+this.state.role);
                break;
            case 'login':
                this.setState({isLoginTab: true, error: null, username: '', password: ''});
                event.preventDefault();
                break;
            case 'signup':
                this.setState({isLoginTab: false, error: null, username: '', password: ''});
                event.preventDefault();
                break;
            default: break;
        }
    }

    handleSubmit(event) {
        event.preventDefault();

        if (!this.state.isValid) {
            this.setState({error: 'Your username or your password is incorrect'});
        } else if (!this.state.isCompleted) {
            this.setState({error: 'Please complete all fileds'});
        } else if(this.state.isLoginTab){
            let data = {
                'username': this.state.username,
                'password': this.state.password
            };

            fetch(API_LOGIN, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }).then(
                res => res.json(),
            ).then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    }, () => this.submit());
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error: error.message
                    })
                }
            );
        }else{
            let data = {
                'username': this.state.username,
                'password': this.state.password,
                'confirmed_password': this.state.confirmed_password,
                'name': capitalizeFirstLetter(this.state.name),
                'lastname': capitalizeFirstLetter(this.state.lastname),
                'email': this.state.email,
                'role': this.state.role
            };

            fetch(API_SIGNUP, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }).then(
                res => res.json(),
            ).then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    }, () => this.submit());
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error: error.message
                    })
                }
            );
        }
    }

    submit() {
        if(this.state.items!=null) {
            switch(this.state.items.message) {
                case 'error : wrong username or password':
                    this.setState({error: 'Wrong username or password.'});
                    console.log(this.state.items.message);
                    break;
                case 'authenticated':
                    console.log(this.state.items.message);
                    this.setState({error: null});
                    this.setState({loggedIn: true }, () => console.log(this.state));
                    sessionStorage.setItem('token', this.state.items.token);
                    sessionStorage.setItem('password', this.state.password);
                    sessionStorage.setItem('username', this.state.username);


                    console.log("le token est :"+ sessionStorage.getItem('token'))
                    var decode = jwtDecode(sessionStorage.getItem('token'));
                    console.log("le token decode :"+ decode);
                    break;
                case 'user registered':
                    console.log(this.state.items.message);
                    this.setState({error: null});
                    this.displayLoginCard();
                    this.displayToastUserCreated();
                    this.setState({isLoginTab: true});
                    break;

                case 'password not confirmed':
                    console.log(this.state.items.message);
                    window.scrollTo(0, 0);
                    this.setState({error: 'password not confirmed'});
                    break;

                default:
                    console.log(this.state.items.message);
                    window.scrollTo(0, 0)
                    this.setState({error: this.state.items.message});


                    break;
            }
        } else {
            console.log('error!')
        }
    }

    render() {
        return (this.state.loggedIn ? (<Route exact path="/"><Redirect to='/blockument'/></Route>) : (
            <div className='row'>
                <div className='col s12 m6 offset-m3'>
                    <div className='card grey lighten-5'>
                        <form>
                            <ul className='tabs'>
                                <li className='tab col m6 s12'><input id='login' type='submit'
                                                                      className='btn-large white-text col m12 s12' value='Connexion'
                                                                  onClick={this.handleChange}/></li>
                                <li className='tab col m6 s12'><input id='signup' type='submit'
                                                                      className='btn-large white-text col m12 s12'
                                                                  value='Créer un compte' onClick={this.handleChange}/></li>
                            </ul>
                        </form>
                        {this.state.isLoginTab ? this.displayLoginCard() : this.displayCreateUserCard()}
                    </div>
                </div>
            </div>
        ));
    }

    displayLoginCard() {
        return (
            <form onSubmit={this.handleSubmit}>
                {this.state.error != null ? <div className='App-error animated pulse'>{this.state.error}</div> : null}
                <div className='card-content white-text'>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input id='username' type='text' className='validate' value={this.state.username}
                                   onChange={this.handleChange}/>
                            <label htmlFor='username'>Identifiant</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input id='password' type='password' className='validate' value={this.state.password}
                                   onChange={this.handleChange}/>
                            <label htmlFor='password'>Mot de passe</label>
                        </div>
                    </div>
                </div>
                <div className='card-action'>
                    <button type='submit' className='btn-flat teal-text'>Connexion<i className='material-icons right'>send</i></button>
                </div>
            </form>
        )
    }

    displayCreateUserCard() {
        return (
            <form onSubmit={this.handleSubmit}>
                {this.state.error != null ? <div className='App-error animated pulse'>{this.state.error}</div> : null}
                <div className='card-content white-text'>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input required id='username' type='text' className='validate' value={this.state.username}
                                   onChange={this.handleChange}/>
                            <label  htmlFor='username'>Identifiant</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input required pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,32}$" title="Au moins 8 caractères comprenant majuscule, minuscule et chiffre"id='password' type='password' className='validate' value={this.state.password}
                                   onChange={this.handleChange}/>
                            <label htmlFor='password'>Mot de passe</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input required id='confirmed_password' type='password' className='validate'
                                   value={this.state.confirmed_password} onChange={this.handleChange}/>
                            <label htmlFor='confirmed_password'>Confirmez votre mot de passe</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input required id='name' type='text' className='validate' value={this.state.name}
                                   onChange={this.handleChange}/>
                            <label htmlFor='name'>Prénom</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input required id='lastname' type='text' className='validate' value={this.state.lastname}
                                   onChange={this.handleChange}/>
                            <label htmlFor='lastname'>Nom</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='input-field col s12'>
                            <input required id='email' type='email' className='validate' value={this.state.email}
                                   onChange={this.handleChange}/>
                            <label htmlFor='email'>Email</label>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='input-field col s12 left-align'>
                            <p>
                                <label>
                                    <input id ="role" className="with-gap" value="patient" c onChange={this.handleChange} name="group1" type="radio" required />
                                    <span>Patient</span>
                                  </label>
                                </p>
                                <p>
                                  <label>
                                    <input id ="role" className="with-gap" value="pharmacien"  onChange={this.handleChange} name="group1" type="radio" />
                                    <span>Pharmacien</span>
                                  </label>
                                </p>
                                <p>
                                  <label>
                                    <input id ="role" className="with-gap" value="medecin" onChange={this.handleChange} name="group1" type="radio" />
                                    <span>Medecin</span>
                                  </label>
                                </p>
                            <label className="active" htmlFor='role'>Role</label>
                        </div>
                    </div>


                    <div className='row'>
                        <div className='input-field col s12'>
                            <p>
                               <label>
                                    <input type="checkbox" required />
                                    <span>En vous inscrivant vous acceptez nos conditions générales.</span>
                                </label>
                            </p>
                        </div>
                    </div>
                </div>
                <div className='card-action'>
                    <button type='submit' className='btn-flat teal-text'>Valider<i className='material-icons right'>send</i></button>
                </div>
            </form>
        )
    }

    displayToastUserCreated() {
      return (
        window.M.toast({html: 'Votre compte a bien été créé !'})
      )
    }
}

export default Login;