export const API = 'https://api.choumarin.knunes.fr';
export const API_LOGIN = API + '/login'
export const API_SIGNUP = API + '/signup'
export const API_BLOCKUMENTS = API + '/get_blockument'
export const API_CREATE = API + '/blockument'
export const CHECK_BLOCKCHAIN = API + '/check_blockchain'
export const NOTIFY_DOWNLOAD = API + '/notify_download'
export const SET_SHARED = API + '/set_shared'



export const BLOCKUMENT_TYPES = [{type: 'Ordonnance', id:'ordonnance'}, {type: 'Compte rendu', id:'compte_rendu'}, {type: 'Certificat médical', id:'certificat_medical'}]