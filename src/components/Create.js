import React from 'react';
import  { BrowserRouter as Router, Route } from 'react-router-dom'

import {API_CREATE} from '../config';
import Blockument from './Blockument';


class Create extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submited: false,
      blockument_type: window.location.href.split("#")[1],
      lastname: "",
      firstname: "",
      age: "",
      weight: "",
      order: {
        advice: "",
        prescription: [{
          drug: "",
          quantity: "",
          duration: "",
          period: "",
          moment: {morning: false, midday: false, evening: false},
          detail: {NS: false, AS: false, QSP: false, NR: false},
        }, {
          drug: "",
          quantity: "",
          duration: "",
          period: "",
          moment: {morning: false, midday: false, evening: false},
          detail: {NS: false, AS: false, QSP: false, NR: false},
        }, {
          drug: "",
          quantity: "",
          duration: "",
          period: "",
          moment: {morning: false, midday: false, evening: false},
          detail: {NS: false, AS: false, QSP: false, NR: false},
        }],
      },
      report: {
        report: "",
      },
      sick_note: {
        note: "",
      } 
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }


  render() {
    if(this.state.submited) {
      return ( 
        <Router>
            <Route component={Blockument}/>
        </Router>
      );
    } else {
      switch(this.state.blockument_type) {
        case 'ordonnance':
          return(this.getOrderForm());
        case 'compte_rendu':
          return(this.getReportForm());
        case 'certificat_medical':
          return(this.getSlickNoteForm()); 
        default : return('Nothing');
      }
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    let data = {
      username:sessionStorage.getItem('username'),
      password: sessionStorage.getItem('password'),
      type: this.state.blockument_type,
      content: JSON.stringify({
        lastname: this.state.lastname,
        firstname: this.state.firstname,
        age: this.state.age,
        weight: this.state.weight,
        order : this.state.order,
        report: this.state.report,
        sick_note: this.state.sick_note,
      })
    };
    fetch(API_CREATE, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    }).then(
      res => res.json(),
    ).then(
      (result) => {
        this.setState({submited: true});
        this.render();
      },
      (error) => {
        console.log(error.message);
      }
    );
  }

  handleChange(event) {
    let state = this.state
    let order = this.state.order;
    let report = this.state.report;
    let sick_note = this.state.sick_note;

    switch (event.target.id) {
      case 'last_name':
        state.lastname = event.target.value;
        break;
      case 'first_name':
        state.firstname = event.target.value;
        break;
      case 'age':
        state.age = event.target.value;
        break;
      case 'weight':
        state.weight = event.target.value;
        break;
      case 'advice':
        order.advice = event.target.value;
        break;
      case 'report':
        report.report = event.target.value;
        break;  
      case 'sick_note':
        sick_note.sick_note = event.target.value;
        break;    
      case 'drug_1':
        order.prescription[0].drug = event.target.value;
        break;
      case 'quantity_1':
        order.prescription[0].quantity = event.target.value;
        break;
      case 'duration_1':
        order.prescription[0].duration = event.target.value;
        break;
      case 'period_1':
        order.prescription[0].period = event.target.value;
        break;
      case 'morning_1':
        order.prescription[0].moment.morning = !order.prescription[0].moment.morning;
        break;
      case 'midday_1':
        order.prescription[0].moment.midday = !order.prescription[0].moment.midday;
        break;  
      case 'evening_1':
        order.prescription[0].moment.evening = !order.prescription[0].moment.evening;
        break;           
      case 'ns_1':
        order.prescription[0].detail.ns = !order.prescription[0].detail.ns;
        break;
      case 'as_1':
        order.prescription[0].detail.as = !order.prescription[0].detail.as;
        break;
      case 'qsp_1':
        order.prescription[0].detail.qsp = !order.prescription[0].detail.qsp;
        break; 
      case 'nr_1':
        order.prescription[0].detail.nr = !order.prescription[0].detail.nr;
        break;  
      case 'drug_2':
        order.prescription[1].drug = event.target.value;
        break;
      case 'quantity_2':
        order.prescription[1].quantity = event.target.value;
        break;
      case 'duration_2':
        order.prescription[1].duration = event.target.value;
        break;
      case 'period_2':
        order.prescription[1].period = event.target.value;
        break;
      case 'morning_2':
        order.prescription[1].moment.morning = !order.prescription[1].moment.morning;
        break;
      case 'midday_2':
        order.prescription[1].moment.midday = !order.prescription[1].moment.midday;
        break;  
      case 'evening_2':
        order.prescription[1].moment.evening = !order.prescription[1].moment.evening;
        break;           
      case 'ns_2':
        order.prescription[1].detail.ns = !order.prescription[1].detail.ns;
        break;
      case 'as_2':
        order.prescription[1].detail.as = !order.prescription[1].detail.as;
        break;
      case 'qsp_2':
        order.prescription[1].detail.qsp = !order.prescription[1].detail.qsp;
        break; 
      case 'nr_2':
        order.prescription[1].detail.nr = !order.prescription[1].detail.nr;
        break;
      case 'drug_3':
        order.prescription[2].drug = event.target.value;
        break;
      case 'quantity_3':
        order.prescription[2].quantity = event.target.value;
        break;
      case 'duration_3':
        order.prescription[2].duration = event.target.value;
        break;
      case 'period_3':
        order.prescription[2].period = event.target.value;
        break;
      case 'morning_3':
        order.prescription[2].moment.morning = !order.prescription[2].moment.morning;
        break;
      case 'midday_3':
        order.prescription[2].moment.midday = !order.prescription[2].moment.midday;
        break;  
      case 'evening_3':
        order.prescription[2].moment.evening = !order.prescription[2].moment.evening;
        break;           
      case 'ns_3':
        order.prescription[2].detail.ns = !order.prescription[2].detail.ns;
        break;
      case 'as_3':
        order.prescription[2].detail.as = !order.prescription[2].detail.as;
        break;
      case 'qsp_3':
        order.prescription[2].detail.qsp = !order.prescription[2].detail.qsp;
        break; 
      case 'nr_3':
        order.prescription[2].detail.nr = !order.prescription[2].detail.nr;
        break;    
      default: break;
    }
    this.setState({lastname: state.lastname, firstname: state.firstname, age: state.age, weight: state.width, order: order, report: report, sick_note: sick_note});
  }

  getOrderForm() {
  	return(
  	  <div>
        <nav>
          <div class='nav-wrapper teal lighten-5'>
              <ul class='left'>
                  <li><a class='teal-text' href="blockument"><i class="material-icons left icon-teal">arrow_back</i></a></li>
              </ul>
              <ul id='nav-mobile' class='right'>
                  <li><a class='teal-text' onClick={this.logOut}>Déconnexion</a></li>
              </ul>
          </div>
        </nav>
        <form onSubmit={this.handleSubmit}>
          <div class='info_advice margin-bottom-5' style={{position: 'absolute', 
                                                            width: '24em', 
                                                            height: '44em',
                                                            margin: '1em'}}>
            <div class='left-align light-teal-background light-teal-border padding-2 margin-bottom-1' style={{height: '24em'}}>
              <h6 class='teal-text flow-text margin-bottom-1'>Informations patient</h6>
              <label for='last_name' class='teal-text'>Nom</label>
              <input id='last_name' type='text' class='custom_input height_13 light-teal-border' required onChange={this.handleChange}/>
              <label for='first_name' class='teal-text'>Prénom</label>
              <input id='first_name' type='text' class='custom_input height_13 light-teal-border' required onChange={this.handleChange}/>
              <div class='row margin-bottom-0'>
                <div class='col m3 s3 padding-left-0'>
                  <label for='age' class='teal-text'>Age</label>
                  <input id='age' type='number' class='custom_input light-teal-border margin-bottom-0' onChange={this.handleChange}/>
                </div>
                <div class='col m3 s3 padding-right-0'>
                  <label for='weight' class='teal-text'>Poids</label>
                  <input id='weight' type='number' class='custom_input light-teal-border margin-bottom-0' onChange={this.handleChange}/>
                </div>
                <div class='col m6 s6 padding-left-1 padding-top-1 valign-wrapper'><p class='teal-text'>Kg</p></div>
              </div>
            </div>

            <div class='left-align light-teal-background light-teal-border padding-2' style={{height: '19em'}}>
              <h6 class='teal-text flow-text margin-bottom-1'>Conseil</h6>
              <textarea id='advice' class='custom_textarea light-teal-border' style={{width: '100%',
                                                                          height: '10em',
                                                                          backgroundColor: 'white',
                                                                          boxSizing: 'border-box'}} onChange={this.handleChange}/>
            </div>
          </div>

          <div class='left-align light-teal-background light-teal-border padding-2' style={{position: 'absolute',
                                                                                            height: '44em',
                                                                                            margin: '1em',
                                                                                            marginLeft: '26em',
                                                                                            marginBottom: '5em'}}>
            <h6 class='teal-text flow-text margin-bottom-1'>Produits prescrits</h6>

            <div class='row valign-wrapper'>
              <div class='col m3'>
                <label for='drug_1' class='teal-text'>Médicament</label>
                <input id='drug_1' type='text' class='custom_input light-teal-border' onChange={this.handleChange}/>
              </div>
              <div class='col m2'>
                <label for='quantity_1' class='teal-text'>Quantité</label>
                <input id='quantity_1' type='number' class='custom_input light-teal-border' onChange={this.handleChange}/>
              </div>
              <div class='col m2'>
                <label for='duration_1' class='teal-text'>Durée</label>
                <input id='duration_1' type='number' class='custom_input light-teal-border' onChange={this.handleChange}/>
              </div>
              <div class='col m2'>
                <p>
                  <label>
                    <input class='with-gap' type='radio' name='period_1' id='period_1' value='day' onChange={this.handleChange}/>
                    <span class='teal-text'>Jour</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input class='with-gap' type='radio' name='period_1' id='period_1' value='week' onChange={this.handleChange}/>
                    <span class='teal-text'>Semaine</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input class='with-gap' type='radio' name='period_1' id='period_1' value='month' onChange={this.handleChange}/>
                    <span class='teal-text'>Mois</span>
                  </label>
                </p>
              </div>
              <div class='col m2'>
                <p>
                  <label>
                    <input type='checkbox' id='morning_1' onChange={this.handleChange}/>
                    <span class='teal-text'>Matin</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='midday_1' onChange={this.handleChange}/>
                    <span class='teal-text'>Midi</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='evening_1' onChange={this.handleChange}/>
                    <span class='teal-text'>Soir</span>
                  </label>
                </p>
              </div>
              <div class='col m2'>
                <p>
                  <label>
                    <input type='checkbox' id='ns_1' onChange={this.handleChange}/>
                    <span class='teal-text'>NS</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='as_1' onChange={this.handleChange}/>
                    <span class='teal-text'>AS</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='qsp_1' onChange={this.handleChange}/>
                    <span class='teal-text'>QSP</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='nr_1' onChange={this.handleChange}/>
                    <span class='teal-text'>NR</span>
                  </label>
                </p>
              </div>
            </div>

            <div class='row valign-wrapper'>
              <div class='col m3'>
                <label for='drug_2' class='teal-text'>Médicament</label>
                <input id='drug_2' type='text' class='custom_input light-teal-border' onChange={this.handleChange}/>
              </div>
              <div class='col m2'>
                <label for='quantity_2' class='teal-text'>Quantité</label>
                <input id='quantity_2' type='number' class='custom_input light-teal-border' onChange={this.handleChange}/>
              </div>
              <div class='col m2'>
                <label for='duration_2' class='teal-text'>Durée</label>
                <input id='duration_2' type='number' class='custom_input light-teal-border' onChange={this.handleChange}/>
              </div>
              <div class='col m2'>
                <p>
                  <label>
                    <input class='with-gap' type='radio' name='period_2' id='period_2' value='day' onChange={this.handleChange}/>
                    <span class='teal-text'>Jour</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input class='with-gap' type='radio' name='period_2' id='period_2' value='week' onChange={this.handleChange}/>
                    <span class='teal-text'>Semaine</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input class='with-gap' type='radio' name='period_2' id='period_2' value='month' onChange={this.handleChange}/>
                    <span class='teal-text'>Mois</span>
                  </label>
                </p>
              </div>
              <div class='col m2'>
                <p>
                  <label>
                    <input type='checkbox' id='morning_2' onChange={this.handleChange}/>
                    <span class='teal-text'>Matin</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='midday_2' onChange={this.handleChange}/>
                    <span class='teal-text'>Midi</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='evening_2' onChange={this.handleChange}/>
                    <span class='teal-text'>Soir</span>
                  </label>
                </p>
              </div>
              <div class='col m2'>
                <p>
                  <label>
                    <input type='checkbox' id='ns_2' onChange={this.handleChange}/>
                    <span class='teal-text'>NS</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='as_2' onChange={this.handleChange}/>
                    <span class='teal-text'>AS</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='qsp_2' onChange={this.handleChange}/>
                    <span class='teal-text'>QSP</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='nr_2' onChange={this.handleChange}/>
                    <span class='teal-text'>NR</span>
                  </label>
                </p>
              </div>
            </div>

            <div class='row valign-wrapper'>
              <div class='col m3'>
                <label for='drug_3' class='teal-text'>Médicament</label>
                <input id='drug_3' type='text' class='custom_input light-teal-border' onChange={this.handleChange}/>
              </div>
              <div class='col m2'>
                <label for='quantity_3' class='teal-text'>Quantité</label>
                <input id='quantity_3' type='number' class='custom_input light-teal-border' onChange={this.handleChange}/>
              </div>
              <div class='col m2'>
                <label for='duration_3' class='teal-text'>Durée</label>
                <input id='duration_3' type='number' class='custom_input light-teal-border' onChange={this.handleChange}/>
              </div>
              <div class='col m2'>
                <p>
                  <label>
                    <input class='with-gap' type='radio' name='period_3' id='period_3' value='day' onChange={this.handleChange}/>
                    <span class='teal-text'>Jour</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input class='with-gap' type='radio' name='period_3' id='period_3' value='week' onChange={this.handleChange}/>
                    <span class='teal-text'>Semaine</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input class='with-gap' type='radio' name='period_3' id='period_3' value='month' onChange={this.handleChange}/>
                    <span class='teal-text'>Mois</span>
                  </label>
                </p>
              </div>
              <div class='col m2'>
                <p>
                  <label>
                    <input type='checkbox' id='morning_3' onChange={this.handleChange}/>
                    <span class='teal-text'>Matin</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='midday_3' onChange={this.handleChange}/>
                    <span class='teal-text'>Midi</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='evening_3' onChange={this.handleChange}/>
                    <span class='teal-text'>Soir</span>
                  </label>
                </p>
              </div>
              <div class='col m2'>
                <p>
                  <label>
                    <input type='checkbox' id='ns_3' onChange={this.handleChange}/>
                    <span class='teal-text'>NS</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='as_3' onChange={this.handleChange}/>
                    <span class='teal-text'>AS</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='qsp_3' onChange={this.handleChange}/>
                    <span class='teal-text'>QSP</span>
                  </label>
                </p>
                <p>
                  <label>
                    <input type='checkbox' id='nr_3' onChange={this.handleChange}/>
                    <span class='teal-text'>NR</span>
                  </label>
                </p>
              </div>
            </div>
            
          </div>
          <button class='btn white-text margin-bottom-1' type='submit' style={{position: 'absolute',
                                                                                marginTop: '49em'}}>Valider</button>
        </form>
      </div>	
  	)
  }

  getReportForm() {
    return (
      <div>
        <nav>
          <div class='nav-wrapper teal lighten-5'>
              <ul class='left'>
                  <li><a class='teal-text' href="blockument"><i class="material-icons left icon-teal">arrow_back</i></a></li>
              </ul>
              <ul id='nav-mobile' class='right'>
                  <li><a class='teal-text' onClick={this.logOut}>Déconnexion</a></li>
              </ul>
          </div>
        </nav>
        <form onSubmit={this.handleSubmit}>
            <div class='left-align light-teal-background light-teal-border padding-2 margin-bottom-1' style={{position: 'absolute', 
                                                                                                              height: '25em',
                                                                                                              width: '30em',
                                                                                                              margin: '1em',
                                                                                                              marginBottom: '5em'}}>
              <h6 class='teal-text flow-text margin-bottom-1'>Informations patient</h6>
              <label for='last_name' class='teal-text'>Nom</label>
              <input id='last_name' type='text' class='custom_input height_13 light-teal-border' required onChange={this.handleChange}/>
              <label for='first_name' class='teal-text'>Prénom</label>
              <input id='first_name' type='text' class='custom_input height_13 light-teal-border' required onChange={this.handleChange}/>
              <div class='row margin-bottom-0'>
                <div class='col m3 s3 padding-left-0'>
                  <label for='age' class='teal-text'>Age</label>
                  <input id='age' type='number' class='custom_input light-teal-border margin-bottom-0' onChange={this.handleChange}/>
                </div>
                <div class='col m3 s3 padding-right-0'>
                  <label for='weight' class='teal-text'>Poids</label>
                  <input id='weight' type='number' class='custom_input light-teal-border margin-bottom-0' onChange={this.handleChange}/>
                </div>
                <div class='col m6 s6 padding-left-1 padding-top-1 valign-wrapper'><p class='teal-text'>Kg</p></div>
              </div>
            </div>

            <div class='left-align light-teal-background light-teal-border padding-2' style={{position: 'absolute', 
                                                                                              height: '25em',
                                                                                              width: '52em',
                                                                                              margin: '1em',
                                                                                              marginLeft: '32em',
                                                                                              marginBottom: '5em'}}>
              <h6 class='teal-text flow-text margin-bottom-1'>Compte rendu</h6>
              <textarea id='report' class='custom_textarea light-teal-border' style={{width: '100%',
                                                                          height: '16em',
                                                                          backgroundColor: 'white',
                                                                          boxSizing: 'border-box'}} onChange={this.handleChange}/>
          </div>
          <button class='btn white-text margin-bottom-1' type='submit' style={{position: 'absolute',
                                                                                marginTop: '29em'}}>Valider</button>
        </form>
      </div>
    )
  }

  getSlickNoteForm() {
    return (
      <div>
        <nav>
          <div class='nav-wrapper teal lighten-5'>
              <ul class='left'>
                  <li><a class='teal-text' href="blockument"><i class="material-icons left icon-teal">arrow_back</i></a></li>
              </ul>
              <ul id='nav-mobile' class='right'>
                  <li><a class='teal-text' onClick={this.logOut}>Déconnexion</a></li>
              </ul>
          </div>
        </nav>
        <form onSubmit={this.handleSubmit}>
            <div class='left-align light-teal-background light-teal-border padding-2 margin-bottom-1' style={{position: 'absolute', 
                                                                                                              height: '25em',
                                                                                                              width: '30em',
                                                                                                              margin: '1em',
                                                                                                              marginBottom: '5em'}}>
              <h6 class='teal-text flow-text margin-bottom-1'>Informations patient</h6>
              <label for='last_name' class='teal-text'>Nom</label>
              <input id='last_name' type='text' class='custom_input height_13 light-teal-border' required onChange={this.handleChange}/>
              <label for='first_name' class='teal-text'>Prénom</label>
              <input id='first_name' type='text' class='custom_input height_13 light-teal-border' required onChange={this.handleChange}/>
              <div class='row margin-bottom-0'>
                <div class='col m3 s3 padding-left-0'>
                  <label for='age' class='teal-text'>Age</label>
                  <input id='age' type='number' class='custom_input light-teal-border margin-bottom-0' onChange={this.handleChange}/>
                </div>
                <div class='col m3 s3 padding-right-0'>
                  <label for='weight' class='teal-text'>Poids</label>
                  <input id='weight' type='number' class='custom_input light-teal-border margin-bottom-0' onChange={this.handleChange}/>
                </div>
                <div class='col m6 s6 padding-left-1 padding-top-1 valign-wrapper'><p class='teal-text'>Kg</p></div>
              </div>
            </div>

            <div class='left-align light-teal-background light-teal-border padding-2' style={{position: 'absolute', 
                                                                                              height: '25em',
                                                                                              width: '52em',
                                                                                              margin: '1em',
                                                                                              marginLeft: '32em',
                                                                                              marginBottom: '5em'}}>
              <h6 class='teal-text flow-text margin-bottom-1'>Certificat médical</h6>
              <textarea id='advice' class='custom_textarea light-teal-border' style={{width: '100%',
                                                                          height: '16em',
                                                                          backgroundColor: 'white',
                                                                          boxSizing: 'border-box'}} onChange={this.handleChange}/>
          </div>
          <button class='btn white-text margin-bottom-1' type='submit' style={{position: 'absolute',
                                                                                marginTop: '29em'}}>Valider</button>
        </form>
      </div>
    )
  }

  logOut() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('password');
    window.location.reload()
  }
}


export default Create;