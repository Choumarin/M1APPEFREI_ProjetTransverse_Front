import React from 'react';
import Coverflow from 'react-coverflow';
import { StyleRoot } from 'radium';
import { convertTimestamp } from './utils'
import {API_BLOCKUMENTS, BLOCKUMENT_TYPES} from '../config';
import { isMedecin } from './utils';


class Blockument extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      items: null,
      isMyBlockument: true,
      isCreate: false,
      isConfiguration: false,
      blockument_index: 1,
    };
      this.getBlockumentList();
  }

  getBlockumentList() {
    let data = {
      'username':sessionStorage.getItem('username'),
      'password': sessionStorage.getItem('password')
    };

    fetch(API_BLOCKUMENTS, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },                
      body: JSON.stringify(data)
    }).then(
      res => res.json(),
    ).then(
      (result) => {
        console.log(result)
        this.setState({items : result});   
      },
      (error) => {
        this.setState({error: error.message});
      }
    );
  }

  render() {
    return (
      <div className='blockument'>
        <nav>
          <div className='nav-wrapper teal lighten-5'>
            <ul className='left'>
              <li><a className='teal-text' onClick={() => this.setState({isMyBlockument: true, isCreate: false, isConfiguration: false})}>Mes Blockuments</a></li>
              {isMedecin() ? <li><a className='teal-text' onClick={() => this.setState({isMyBlockument: false, isCreate: true, isConfiguration: false})}>Créer</a></li> :''}
            </ul>
            <ul id='nav-mobile' className='right'>
              <li><a className='teal-text' onClick={this.logOut}>Déconnexion</a></li>
            </ul>
          </div>
        </nav>
        { this.state.isMyBlockument ? this.displayBlockuments() : this.state.isCreate ? this.displayCreate() : this.displayConfig() }
      </div>
    );
  }

  displayBlockuments() {
    if(this.state.items === null) {
      return (
        <div className='preloader-wrapper big active margin-top-5'>
          <div className='spinner-layer spinner-teal-only'>
            <div className='circle-clipper left'>
              <div className='circle'></div>
            </div><div className='gap-patch'>
              <div className='circle'></div>
            </div><div className='circle-clipper right'>
              <div className='circle'></div>
            </div>
          </div>
        </div>
      );
    }
    else if(this.state.items.blockuments_list.length === 0) {
      return this.displayNoBlockuments();
    } else {
      return(
        <div className='row'> 
          { this.state.items.blockuments_list.map( (blockument, i) => {
            let type = '';
            BLOCKUMENT_TYPES.map((b) => {
              if(b.id === blockument.blockument.type) {
                type = b.type;
              }
            });
            return (
              <a key={i} href={'/blockumentdetails#'+blockument.blockument.id}>
                <div className='card col m3 s9 margin-right-1 margin-left-5'>
                  <div className="card-content">
                    <p className='margin-bottom-4'>
                      <span className='teal-text left-align'>
                        <i className="material-icons left icon-teal margin-right-0">account_circle</i>Dr. {blockument.blockument.author.lastname}
                      </span>
                      <i className='grey-text margin-left-3 xx-small-size'>{convertTimestamp(blockument.blockument.last_update_date)}</i>
                    </p>
                    <p>
                      <div className='margin-bottom-2 padding-bottom-4 padding-top-4 light-teal-background teal-text'>{type}</div>
                    </p>
                  </div>
                </div>
              </a>
            )}
          )}
        </div>
      )
    }
  }
 
  displayCreate() {
    return(
      <StyleRoot>
        <Coverflow
          displayQuantityOfSide={3}
          navigation 
          infiniteScroll
          enableHeading
          enableScroll={false}
          media={{
            '@media (max-width: 900px)': {
              margin: 'auto',
              position: 'absolute',
              top: '150px',
              bottom: '0%',
              width:'100%'
            },
            '@media (min-width: 900px)': {
              margin: 'auto',
              position: 'absolute',
              top: '214px',
              bottom: '0%',
              width:'100%'
            }
          }}>
          {
            BLOCKUMENT_TYPES.map((blockument,i) => 
              <div key={i} className='card'>
                  <div className='light-teal-background'>
                    <div className='padding-bottom-4 padding-top-6 teal-text'>{blockument.type}</div>
                    <div className='padding-1'>
                      <a href={'/create#'+blockument.id}><button className='waves-effect waves-light btn-small x-small-button'>Choisir</button></a>
                    </div>
                  </div>
              </div>
            ) 
          }
        </Coverflow>
      </StyleRoot>
    )
  }

  displayConfig() {
    return(<div>Configuration</div>)
  }

  displayNoBlockuments() {
    return(<h4 style={{color: '#4db6ac'}} className='margin-top-5'>Vous n'avez pas de Blockuments</h4>)
  }

  logOut() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('password');
    window.location.reload()
  }
}

export default Blockument;